<?php


if ( isset( $_FILES['csv'] ) ) {

	if ( ( $csv = fopen( $_FILES['csv']['tmp_name'], 'r' ) ) !== false ) {

		$first = true;
		$array = array();
		while ( ( $row = fgetcsv( $csv, 0, ',' ) ) !== false ) {
			if ( $first ) {
				$first = false;
				continue;
			}
			$row = explode( ';', $row[0] );
			$lat = $row[1];
			$lon = $row[0];
			$lat = DMStoDD( $lat );
			$lon = DMStoDD( $lon );

			$array[] = array(
				'Latitude'  => $lat,
				'Longitude' => $lon,
			);
		}
		fclose( $csv );

		download_send_headers( 'latlonconvert_' . date( 'Y-m-d' ) . '.csv' );
		echo array2csv( $array );
		die();
	}
}

function array2csv( array &$array ) {
	if ( count( $array ) == 0 ) {
		return null;
	}
	ob_start();
	$df = fopen( 'php://output', 'w' );
	fputcsv( $df, array_keys( reset( $array ) ) );
	foreach ( $array as $row ) {
		fputcsv( $df, $row );
	}
	fclose( $df );
	return ob_get_clean();
}

function download_send_headers( $filename ) {
	// disable caching
	$now = gmdate( 'D, d M Y H:i:s' );
	header( 'Expires: Tue, 03 Jul 2001 06:00:00 GMT' );
	header( 'Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate' );
	header( "Last-Modified: {$now} GMT" );

	// force download
	header( 'Content-Type: application/force-download' );
	header( 'Content-Type: application/octet-stream' );
	header( 'Content-Type: application/download' );

	// disposition / encoding on response body
	header( "Content-Disposition: attachment;filename={$filename}" );
	header( 'Content-Transfer-Encoding: binary' );
}

function DMStoDD( $input ) {
	$deg    = ' ';
	$min    = ' ';
	$sec    = ' ';
	$inputM = ' ';

	$direction = 1;

	for ( $i = 0; $i < strlen( $input ); $i++ ) {
		$tempD = $input[ $i ];

		if ( $tempD == iconv( 'UTF-8', 'ISO-8859-1//TRANSLIT', '°' ) ) {
			$newI   = $i + 1;
			$inputM = substr( $input, $newI, -1 );
			break;
		}//close if degree

		$deg .= $tempD;

	}//close for degree

	for ( $j = 0; $j < strlen( $inputM ); $j++ ) {
		$tempM = $inputM[ $j ];

		if ( $tempM == "'" ) {
			$newI = $j + 1;
			$sec  = substr( $inputM, $newI, -1 );
			break;
		}
		 $min .= $tempM;
	}//close for min

	$dir = strlen( $input ) - 1;
	$dir = $input[ $dir ];
	if ( 'S' === $dir || 'W' === $dir ) {
			$direction = -1;
	}

	$latlon = $deg + ( $min / 60 ) + ( $sec / 3600 );

	return $latlon * $direction;
}

?>
<head>
	<title>Coordinate Converter from DMS to DD</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
	<div class="container mt-3">
		<h1 class="h2 text-center mb-3">Coordinate Converter from DMS to DD</h1>
		<div class="bg-light p-4 mb-4">
		<p>Bulk Coordinate convertion using CSV as input/output format</p>
		<p>Example DMS (43°19'59.1096"N)</p>
		<p>Example DD (43.333086)</p>
		</div>
		<form enctype="multipart/form-data" action="" method="post">
		<div class="mb-3">
		<label class="form-label" for="csv">CSV File</label>
			<input id="csv" type="file" name="csv">
		</div>
			<button type="submit"  class="btn btn-primary">Convert</button>
		</form>
	</div>
</body>


